package rpc

import (
	"fmt"

	core "github.com/tendermint/tendermint/rpc/core/types"
	tm "github.com/tendermint/tendermint/types"
)

func (r *RPC) FetchValidators(height int64) ([]*tm.Validator, error) {
	url := fmt.Sprintf("%s/validators?height=%d", r.address, height)

	raw, err := r.call(url)
	if err != nil {
		return nil, err
	}
	if raw == nil {
		return nil, nil
	}

	var result core.ResultValidators
	if err := r.cdc.UnmarshalJSON(raw, &result); err != nil {
		return nil, err
	}

	return result.Validators, nil
}
