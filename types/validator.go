package types

import (
	"time"
)

type ValidatorDescription struct {
	Moniker  string `json:"moniker,omitempty" bson:"moniker"`
	Identity string `json:"identity,omitempty" bson:"identity"`
	Website  string `json:"website,omitempty" bson:"website"`
	Details  string `json:"details,omitempty" bson:"details"`
}

type ValidatorCommission struct {
	Rate          string    `json:"rate,omitempty" bson:"rate"`
	MaxRate       string    `json:"max_rate,omitempty" bson:"max_rate"`
	MaxChangeRate string    `json:"max_change_rate,omitempty" bson:"max_change_rate"`
	UpdatedAt     time.Time `json:"updated_at,omitempty" bson:"updated_at"`
}
