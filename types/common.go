package types

import (
	"github.com/cosmos/cosmos-sdk/types"
)

type Coin struct {
	Denom string `json:"denom,omitempty" bson:"denom"`
	Value int64  `json:"value,omitempty" bson:"value"`
}

type Signature struct {
	PubKey    string `json:"pub_key,omitempty" bson:"pub_key"`
	Signature string `json:"signature,omitempty" bson:"signature"`
}

type Coins []Coin

func NewCoinsFromRaw(coins types.Coins) Coins {
	if coins == nil {
		return nil
	}

	_coins := make(Coins, 0, coins.Len())
	for _, c := range coins {
		_coins = append(_coins, Coin{
			Denom: c.Denom,
			Value: c.Amount.Int64(),
		})
	}

	return _coins
}
