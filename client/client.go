package client

import (
	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/tendermint/tendermint/lite"
	rpc "github.com/tendermint/tendermint/rpc/client"
)

type Client struct {
	context.CLIContext
}

func NewClient(cdc *codec.Codec, client rpc.Client, verifier lite.Verifier) *Client {
	return &Client{
		CLIContext: context.CLIContext{
			Codec:    cdc,
			Client:   client,
			Verifier: verifier,
		},
	}
}
