package types

type Fee struct {
	Amount []Coin `json:"amount,omitempty" bson:"amount"`
	Gas    uint64 `json:"gas,omitempty" bson:"gas"`
}

type Message struct {
	Type    string      `json:"type,omitempty" bson:"type"`
	Log     string      `json:"log,omitempty" bson:"log"`
	Success bool        `json:"success,omitempty" bson:"success"`
	Data    interface{} `json:"data,omitempty" bson:"data"`
}

type Result struct {
	Code      uint32 `json:"code,omitempty" bson:"code"`
	CodeSpace string `json:"code_space,omitempty" bson:"code_space"`
	GasWanted int64  `json:"gas_wanted,omitempty" bson:"gas_wanted"`
	GasUsed   int64  `json:"gas_used,omitempty" bson:"gas_used"`
	Log       string `json:"log,omitempty" bson:"log"`
}
