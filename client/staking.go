package client

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/staking"
)

func (c *Client) QueryDelegationsOfValidator(address sdk.ValAddress) (staking.Delegations, int64, error) {
	params := staking.NewQueryValidatorParams(address)
	bytes, err := c.Codec.MarshalJSON(params)
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", staking.QuerierRoute, staking.QueryValidatorDelegations)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var delegations staking.Delegations
	if err := c.Codec.UnmarshalJSON(res, &delegations); err != nil {
		return nil, 0, err
	}

	return delegations, height, nil
}

func (c *Client) QueryValidators(page, limit int, status string) (staking.Validators, int64, error) {
	params := staking.NewQueryValidatorsParams(page, limit, status)
	bytes, err := c.Codec.MarshalJSON(params)
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", staking.QuerierRoute, staking.QueryValidators)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var validators staking.Validators
	if err := c.Codec.UnmarshalJSON(res, &validators); err != nil {
		return nil, 0, err
	}

	return validators, height, nil
}

func (c *Client) QueryDelegationsOfValidators(addresses []sdk.ValAddress) (staking.Delegations, error) {
	var delegations staking.Delegations
	for _, address := range addresses {
		_delegations, _, err := c.QueryDelegationsOfValidator(address)
		if err != nil {
			return nil, err
		}

		delegations = append(delegations, _delegations...)
	}

	return delegations, nil
}

func (c *Client) QueryAllValidators() (staking.Validators, error) {
	unbonded, _, err := c.QueryValidators(1, 0, sdk.BondStatusUnbonded)
	if err != nil {
		return nil, err
	}

	unbonding, _, err := c.QueryValidators(1, 0, sdk.BondStatusUnbonding)
	if err != nil {
		return nil, err
	}

	bonded, _, err := c.QueryValidators(1, 0, sdk.BondStatusBonded)
	if err != nil {
		return nil, err
	}

	return append(unbonded, append(unbonding, bonded...)...), nil
}
