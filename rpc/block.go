package rpc

import (
	"fmt"

	core "github.com/tendermint/tendermint/rpc/core/types"
	"github.com/tendermint/tendermint/state"
	tm "github.com/tendermint/tendermint/types"
)

func (r *RPC) FetchBlock(height int64) (*tm.Block, error) {
	url := fmt.Sprintf("%s/block?height=%d", r.address, height)

	raw, err := r.call(url)
	if err != nil {
		return nil, err
	}
	if raw == nil {
		return nil, nil
	}

	var result core.ResultBlock
	if err := r.cdc.UnmarshalJSON(raw, &result); err != nil {
		return nil, err
	}

	return result.Block, nil
}

func (r *RPC) FetchBlockResults(height int64) (*state.ABCIResponses, error) {
	url := fmt.Sprintf("%s/block_results?height=%d", r.address, height)

	raw, err := r.call(url)
	if err != nil {
		return nil, err
	}
	if raw == nil {
		return nil, nil
	}

	var result core.ResultBlockResults
	if err := r.cdc.UnmarshalJSON(raw, &result); err != nil {
		return nil, err
	}

	return result.Results, nil
}
