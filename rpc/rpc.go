package rpc

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/cosmos/cosmos-sdk/codec"
	lib "github.com/tendermint/tendermint/rpc/lib/types"
)

type RPC struct {
	address string
	cdc     *codec.Codec
	client  *http.Client
}

func NewRPC(cdc *codec.Codec, address string) *RPC {
	return &RPC{
		address: address,
		cdc:     cdc,
		client:  &http.Client{},
	}
}

func (r *RPC) call(url string) (json.RawMessage, error) {
	resp, err := r.client.Get(url)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err = resp.Body.Close(); err != nil {
			log.Println(err)
		}
	}()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var rr lib.RPCResponse
	if err := r.cdc.UnmarshalJSON(bytes, &rr); err != nil {
		return nil, err
	}

	return rr.Result, nil
}
