package messages

import (
	"github.com/cosmos/cosmos-sdk/x/bank"
	"github.com/tendermint/tendermint/libs/common"

	"gitlab.com/theoctet/cosmos/types"
)

type Send struct {
	FromAddress string      `json:"from_address,omitempty" bson:"from_address"`
	ToAddress   string      `json:"to_address,omitempty" bson:"to_address"`
	Amount      types.Coins `json:"amount,omitempty" bson:"amount"`
}

func NewSendFromRaw(m *bank.MsgSend) *Send {
	return &Send{
		FromAddress: common.HexBytes(m.FromAddress.Bytes()).String(),
		ToAddress:   common.HexBytes(m.ToAddress).String(),
		Amount:      types.NewCoinsFromRaw(m.Amount),
	}
}

type Input struct {
	Address string      `json:"address,omitempty" bson:"address"`
	Coins   types.Coins `json:"coins,omitempty" bson:"coins"`
}

type Output struct {
	Address string      `json:"address,omitempty" bson:"address"`
	Coins   types.Coins `json:"coins,omitempty" bson:"coins"`
}

type MultiSend struct {
	Inputs  []Input  `json:"inputs,omitempty" bson:"inputs"`
	Outputs []Output `json:"outputs,omitempty" bson:"outputs"`
}

func NewMultiSendFromRaw(m *bank.MsgMultiSend) *MultiSend {
	multiSend := MultiSend{
		Inputs:  make([]Input, 0, len(m.Inputs)),
		Outputs: make([]Output, 0, len(m.Outputs)),
	}

	for _, i := range m.Inputs {
		multiSend.Inputs = append(multiSend.Inputs,
			Input{
				Address: common.HexBytes(i.Address.Bytes()).String(),
				Coins:   types.NewCoinsFromRaw(i.Coins),
			})
	}

	for _, o := range m.Outputs {
		multiSend.Outputs = append(multiSend.Outputs,
			Output{
				Address: common.HexBytes(o.Address.Bytes()).String(),
				Coins:   types.NewCoinsFromRaw(o.Coins),
			})
	}

	return &multiSend
}
