package messages

import (
	"github.com/cosmos/cosmos-sdk/x/distribution"
	"github.com/tendermint/tendermint/libs/common"
)

type SetWithdrawAddress struct {
	DelegatorAddress string `json:"delegator_address,omitempty" bson:"delegator_address"`
	WithdrawAddress  string `json:"withdraw_address,omitempty" bson:"withdraw_address"`
}

func NewSetWithdrawAddressFromRaw(m *distribution.MsgSetWithdrawAddress) *SetWithdrawAddress {
	return &SetWithdrawAddress{
		DelegatorAddress: common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		WithdrawAddress:  common.HexBytes(m.WithdrawAddress.Bytes()).String(),
	}
}

type WithdrawDelegatorReward struct {
	DelegatorAddress string `json:"delegator_address,omitempty" bson:"delegator_address"`
	ValidatorAddress string `json:"validator_address,omitempty" bson:"validator_address"`
}

func NewWithdrawDelegatorRewardFromRaw(m *distribution.MsgWithdrawDelegatorReward) *WithdrawDelegatorReward {
	return &WithdrawDelegatorReward{
		DelegatorAddress: common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		ValidatorAddress: common.HexBytes(m.ValidatorAddress.Bytes()).String(),
	}
}

type WithdrawValidatorCommission struct {
	ValidatorAddress string `json:"validator_address,omitempty" bson:"validator_address"`
}

func NewWithdrawValidatorCommissionFromRaw(
	m *distribution.MsgWithdrawValidatorCommission) *WithdrawValidatorCommission {
	return &WithdrawValidatorCommission{
		ValidatorAddress: common.HexBytes(m.ValidatorAddress.Bytes()).String(),
	}
}
