package messages

import (
	"github.com/cosmos/cosmos-sdk/x/slashing"
	"github.com/tendermint/tendermint/libs/common"
)

type Unjail struct {
	Address string `json:"address,omitempty" bson:"address"`
}

func NewUnjailFromRaw(m *slashing.MsgUnjail) *Unjail {
	return &Unjail{
		Address: common.HexBytes(m.ValidatorAddr.Bytes()).String(),
	}
}
