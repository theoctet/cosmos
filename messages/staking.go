package messages

import (
	"github.com/cosmos/cosmos-sdk/x/staking"
	"github.com/tendermint/tendermint/libs/common"

	"gitlab.com/theoctet/cosmos/types"
)

type CreateValidator struct {
	Description       types.ValidatorDescription `json:"description,omitempty" bson:"description"`
	Commission        types.ValidatorCommission  `json:"commission,omitempty" bson:"commission"`
	MinSelfDelegation int64                      `json:"min_self_delegation,omitempty" bson:"min_self_delegation"`
	DelegatorAddress  string                     `json:"delegator_address,omitempty" bson:"delegator_address"`
	ValidatorAddress  string                     `json:"validator_address,omitempty" bson:"validator_address"`
	PubKey            string                     `json:"pub_key,omitempty" bson:"pub_key"`
	Amount            types.Coin                 `json:"amount,omitempty" bson:"amount"`
}

func NewCreateValidatorFromRaw(m *staking.MsgCreateValidator) *CreateValidator {
	return &CreateValidator{
		Description: types.ValidatorDescription{
			Moniker:  m.Description.Moniker,
			Identity: m.Description.Identity,
			Website:  m.Description.Website,
			Details:  m.Description.Details,
		},
		Commission: types.ValidatorCommission{
			Rate:          m.Commission.Rate.String(),
			MaxRate:       m.Commission.MaxRate.String(),
			MaxChangeRate: m.Commission.MaxChangeRate.String(),
		},
		MinSelfDelegation: m.MinSelfDelegation.Int64(),
		DelegatorAddress:  common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		ValidatorAddress:  common.HexBytes(m.ValidatorAddress.Bytes()).String(),
		PubKey:            common.HexBytes(m.PubKey.Bytes()).String(),
		Amount: types.Coin{
			Denom: m.Value.Denom,
			Value: m.Value.Amount.Int64(),
		},
	}
}

type EditValidator struct {
	Description       types.ValidatorDescription `json:"description,omitempty" bson:"description"`
	ValidatorAddress  string                     `json:"validator_address,omitempty" bson:"validator_address"`
	CommissionRate    string                     `json:"commission_rate,omitempty" bson:"commission_rate"`
	MinSelfDelegation int64                      `json:"min_self_delegation,omitempty" bson:"min_self_delegation"`
}

func NewEditValidatorFromRaw(m *staking.MsgEditValidator) *EditValidator {
	editValidator := EditValidator{
		Description: types.ValidatorDescription{
			Moniker:  m.Description.Moniker,
			Identity: m.Description.Identity,
			Website:  m.Description.Website,
			Details:  m.Description.Details,
		},
		ValidatorAddress: common.HexBytes(m.ValidatorAddress.Bytes()).String(),
	}

	if m.CommissionRate != nil {
		editValidator.CommissionRate = m.CommissionRate.String()
	}

	if m.MinSelfDelegation != nil {
		editValidator.MinSelfDelegation = m.MinSelfDelegation.Int64()
	}

	return &editValidator
}

type Delegate struct {
	DelegatorAddress string     `json:"delegator_address,omitempty" bson:"delegator_address"`
	ValidatorAddress string     `json:"validator_address,omitempty" bson:"validator_address"`
	Amount           types.Coin `json:"amount,omitempty" bson:"amount"`
}

func NewDelegateFromRaw(m *staking.MsgDelegate) *Delegate {
	return &Delegate{
		DelegatorAddress: common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		ValidatorAddress: common.HexBytes(m.ValidatorAddress.Bytes()).String(),
		Amount: types.Coin{
			Denom: m.Amount.Denom,
			Value: m.Amount.Amount.Int64(),
		},
	}
}

type BeginRedelegate struct {
	DelegatorAddress    string     `json:"delegator_address,omitempty" bson:"delegator_address"`
	ValidatorSrcAddress string     `json:"validator_src_address,omitempty" bson:"validator_src_address"`
	ValidatorDstAddress string     `json:"validator_dst_address,omitempty" bson:"validator_dst_address"`
	Amount              types.Coin `json:"amount,omitempty" bson:"amount"`
}

func NewBeginRedelegateFromRaw(m *staking.MsgBeginRedelegate) *BeginRedelegate {
	return &BeginRedelegate{
		DelegatorAddress:    common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		ValidatorSrcAddress: common.HexBytes(m.ValidatorSrcAddress.Bytes()).String(),
		ValidatorDstAddress: common.HexBytes(m.ValidatorDstAddress.Bytes()).String(),
		Amount: types.Coin{
			Denom: m.Amount.Denom,
			Value: m.Amount.Amount.Int64(),
		},
	}
}

type Undelegate struct {
	DelegatorAddress string     `json:"delegator_address,omitempty" bson:"delegator_address"`
	ValidatorAddress string     `json:"validator_address,omitempty" bson:"validator_address"`
	Amount           types.Coin `json:"amount,omitempty" bson:"amount"`
}

func NewUndelegateFromRaw(m *staking.MsgUndelegate) *Undelegate {
	return &Undelegate{
		DelegatorAddress: common.HexBytes(m.DelegatorAddress.Bytes()).String(),
		ValidatorAddress: common.HexBytes(m.ValidatorAddress.Bytes()).String(),
		Amount: types.Coin{
			Denom: m.Amount.Denom,
			Value: m.Amount.Amount.Int64(),
		},
	}
}
