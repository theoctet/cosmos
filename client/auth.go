package client

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/auth"
)

func (c *Client) QueryAccount(address sdk.AccAddress) (auth.Account, int64, error) {
	bytes, err := c.Codec.MarshalJSON(auth.NewQueryAccountParams(address))
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", auth.QuerierRoute, auth.QueryAccount)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var account auth.Account
	if res == nil {
		return account, height, nil
	}

	if err := c.Codec.UnmarshalJSON(res, &account); err != nil {
		return nil, 0, err
	}

	return account, height, nil
}
