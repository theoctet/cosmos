package messages

import (
	"github.com/cosmos/cosmos-sdk/x/crisis"
	"github.com/tendermint/tendermint/libs/common"
)

type VerifyInvariant struct {
	Sender              string `json:"sender,omitempty" bson:"sender"`
	InvariantModuleName string `json:"invariant_module_name,omitempty" bson:"invariant_module_name"`
	InvariantRoute      string `json:"invariant_route,omitempty" bson:"invariant_route"`
}

func NewVerifyInvariantFromRaw(m *crisis.MsgVerifyInvariant) *VerifyInvariant {
	return &VerifyInvariant{
		Sender:              common.HexBytes(m.Sender.Bytes()).String(),
		InvariantModuleName: m.InvariantModuleName,
		InvariantRoute:      m.InvariantRoute,
	}
}
