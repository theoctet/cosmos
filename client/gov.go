package client

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/x/gov"
)

func (c *Client) QueryProposals(status gov.ProposalStatus, limit uint64,
	voter, depositor sdk.AccAddress) (gov.Proposals, int64, error) {
	params := gov.NewQueryProposalsParams(status, limit, voter, depositor)
	bytes, err := c.Codec.MarshalJSON(params)
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", gov.QuerierRoute, gov.QueryProposals)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var proposals gov.Proposals
	if err := c.Codec.UnmarshalJSON(res, &proposals); err != nil {
		return nil, 0, err
	}

	return proposals, height, nil
}

func (c *Client) QueryDeposits(id uint64, depositor sdk.AccAddress) (gov.Deposits, int64, error) {
	params := gov.NewQueryDepositParams(id, depositor)
	bytes, err := c.Codec.MarshalJSON(params)
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", gov.QuerierRoute, gov.QueryDeposits)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var deposits gov.Deposits
	if err := c.Codec.UnmarshalJSON(res, &deposits); err != nil {
		return nil, 0, err
	}

	return deposits, height, nil
}

func (c *Client) QueryVotes(id uint64, voter sdk.AccAddress) (gov.Votes, int64, error) {
	params := gov.NewQueryVoteParams(id, voter)
	bytes, err := c.Codec.MarshalJSON(params)
	if err != nil {
		return nil, 0, err
	}

	route := fmt.Sprintf("custom/%s/%s", gov.QuerierRoute, gov.QueryVotes)
	res, height, err := c.QueryWithData(route, bytes)
	if err != nil {
		return nil, 0, err
	}

	var votes gov.Votes
	if err := c.Codec.UnmarshalJSON(res, &votes); err != nil {
		return nil, 0, err
	}

	return votes, height, nil
}
