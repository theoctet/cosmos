package types

import (
	"time"
)

type BlockPreCommit struct {
	Type             uint8     `json:"type,omitempty" bson:"type"`
	Round            int       `json:"round,omitempty" bson:"round"`
	Timestamp        time.Time `json:"timestamp,omitempty" bson:"timestamp"`
	ValidatorAddress string    `json:"validator_address,omitempty" bson:"validator_address"`
	Signature        string    `json:"signature,omitempty" bson:"signature"`
}
