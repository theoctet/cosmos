package messages

import (
	"github.com/cosmos/cosmos-sdk/x/gov"
	"github.com/tendermint/tendermint/libs/common"

	"gitlab.com/theoctet/cosmos/types"
)

type Deposit struct {
	ProposalID uint64      `json:"proposal_id,omitempty" bson:"proposal_id"`
	Depositor  string      `json:"depositor,omitempty" bson:"depositor"`
	Amount     types.Coins `json:"amount,omitempty" bson:"amount"`
}

func NewDepositFromRaw(m *gov.MsgDeposit) *Deposit {
	return &Deposit{
		ProposalID: m.ProposalID,
		Depositor:  common.HexBytes(m.Depositor.Bytes()).String(),
		Amount:     types.NewCoinsFromRaw(m.Amount),
	}
}

type SubmitProposal struct {
	Title          string      `json:"title,omitempty" bson:"title"`
	Description    string      `json:"description,omitempty" bson:"description"`
	ProposalType   string      `json:"proposal_type,omitempty" bson:"proposal_type"`
	ProposalRoute  string      `json:"proposal_route,omitempty" bson:"proposal_route"`
	Proposer       string      `json:"proposer,omitempty" bson:"proposer"`
	InitialDeposit types.Coins `json:"initial_deposit,omitempty" bson:"initial_deposit"`
}

func NewSubmitProposalFromRaw(m *gov.MsgSubmitProposal) *SubmitProposal {
	return &SubmitProposal{
		Title:          m.Content.GetTitle(),
		Description:    m.Content.GetDescription(),
		ProposalType:   m.Content.ProposalType(),
		ProposalRoute:  m.Content.ProposalRoute(),
		Proposer:       common.HexBytes(m.Proposer.Bytes()).String(),
		InitialDeposit: types.NewCoinsFromRaw(m.InitialDeposit),
	}
}

type Vote struct {
	ProposalID uint64 `json:"proposal_id,omitempty" bson:"proposal_id"`
	Voter      string `json:"voter,omitempty" bson:"voter"`
	Option     string `json:"option,omitempty" bson:"option"`
}

func NewVoteFromRaw(m *gov.MsgVote) *Vote {
	return &Vote{
		ProposalID: m.ProposalID,
		Voter:      common.HexBytes(m.Voter.Bytes()).String(),
		Option:     m.Option.String(),
	}
}
