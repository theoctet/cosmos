package types

type ProposalTallyResult struct {
	Yes        int64 `json:"yes,omitempty" bson:"yes"`
	Abstain    int64 `json:"abstain,omitempty" bson:"abstain"`
	No         int64 `json:"no,omitempty" bson:"no"`
	NoWithVeto int64 `json:"no_with_veto,omitempty" bson:"no_with_veto"`
}

type ProposalVote struct {
	Address string `json:"address,omitempty" bson:"address"`
	Option  string `json:"option,omitempty" bson:"option"`
}

type ProposalDeposit struct {
	Address string `json:"address,omitempty" bson:"address"`
	Amount  []Coin `json:"amount,omitempty" bson:"amount"`
}
